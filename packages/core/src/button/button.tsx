import * as React from 'react'
import {useRef, useState} from 'react'
import classNames from 'classnames'
import * as Classes from '../classes';
import {ButtonStyle} from './button.style'
import {isKeyboardClick} from '../keys';
import {Intent, Size} from '../variables';

interface IProps {
  size?: Size;
  intent?: Intent;
  active?;
  disabled?;
  loading?;
  icon?;
  rightIcon?;
  onKeyDown?;
  onKeyUp?;
  onClick?;
  className?: string;
  children?;
}

export const Button = (props: IProps = {}) => {
  const {size, intent, active, disabled, loading, icon, rightIcon, onKeyDown, onKeyUp, onClick, children} = props
  const buttonRef = useRef(null)
  const [isActive, setActive] = useState(false)
  const [currentKeyDown, setCurrentKeyDown] = useState(null)

  const commonButtonProps = () => {
    const styles = ButtonStyle.useStyles()
    const className = classNames(
      props.className,
      styles.button,
      Classes.sizeClass(size),
      Classes.intentClass(intent),
      {
        [Classes.ACTIVE]: active || isActive,
      },
    )
    return {
      className,
      disabled: disabled || loading,
      onKeyDown: handleKeyDown,
      onKeyUp: handleKeyUp,
      onClick: handleClick,
    }
  }

  const renderChildren = () => {
    return [
      <span key="leftIcon">icon</span>,
      <span key="text">
        {children}
      </span>,
      <span key="rightIcon">iconRight</span>,
    ]
  }

  const handleKeyDown = (event: React.KeyboardEvent<any>) => {
    const code = event.which
    if (isKeyboardClick(code) && currentKeyDown !== code) {
      event.preventDefault()
      onKeyDown && onKeyDown()
      buttonRef.current.focus()
      setActive(true)
      setCurrentKeyDown(code)
    }
  }

  const handleKeyUp = (event: React.KeyboardEvent<any>) => {
    if (isKeyboardClick(event.which)) {
      event.preventDefault()
      onKeyUp && onKeyUp()
      buttonRef.current.click()
      setActive(false)
      setCurrentKeyDown(null)
    }
  }

  const handleClick = () => {
    onClick && onClick()
    buttonRef.current.focus()
  }

  return (
    <button
      ref={buttonRef}
      {...commonButtonProps()}
    >
      {renderChildren()}
    </button>
  )
}
