import {gridSize, iconSizeLarge, iconSizeSmall, iconSizeStandard, Intent, isIntentStd} from '../variables';
import {rgba} from '../utils';
import {black, blue1, blue2, green1, green2, intentDanger, intentPrimary, intentStandard, intentSuccess, intentWarning, lightGray1, lightGray2, lightGray4, orange1, orange2, red1, red2, textColor, textColorDisabled, white} from '../colors';

export const borderWidth = 1

export const buttonHeight = gridSize * 3
export const buttonHeightSmall = gridSize * 2.4
export const buttonHeightLarge = gridSize * 4

export const padding = [gridSize / 2, gridSize]
export const paddingSmall = [0, gridSize * 0.7]
export const paddingLarge = [gridSize / 2, gridSize * 1.5]
export const iconSpacing = (buttonHeight - iconSizeStandard) / 2
export const iconSpacingSmall = (buttonHeightSmall - iconSizeSmall) / 2
export const iconSpacingLarge = (buttonHeightLarge - iconSizeLarge) / 2


export const boxShadow = (intent: Intent) => isIntentStd(intent)
  ? [
    {inset: 'inset', color: rgba(black, 0.2), x: 0, y: 0, blur: 0, spread: borderWidth},
    {inset: 'inset', color: rgba(black, 0.1), x: 0, y: -borderWidth, blur: 0, spread: null}]
  : [
    {inset: 'inset', color: rgba(black, 0.4), x: 0, y: 0, blur: 0, spread: borderWidth},
    {inset: 'inset', color: rgba(black, 0.2), x: 0, y: -borderWidth, blur: 0, spread: null}
  ]
export const boxShadowActive = (intent: Intent) => isIntentStd(intent)
  ? [
    {inset: 'inset', color: rgba(black, 0.2), x: 0, y: 0, blur: 0, spread: borderWidth},
    {inset: 'inset', color: rgba(black, 0.1), x: 0, y: 1, blur: 2, spread: null}
  ]
  : [
    {inset: 'inset', color: rgba(black, 0.4), x: 0, y: 0, blur: 0, spread: borderWidth},
    {inset: 'inset', color: rgba(black, 0.2), x: 0, y: 1, blur: 2, spread: null}
  ]

export const gradient = (intent: Intent) => isIntentStd(intent)
  ? `linear-gradient(to bottom, ${rgba(white, 0.6)}, ${rgba(white, 0)})`
  : `linear-gradient(to bottom, ${rgba(white, 0.3)}, ${rgba(white, 0)})`

export const color = (intent: Intent) => isIntentStd(intent) ? textColor : white
export const colorDisabled = (intent: Intent) => isIntentStd(intent) ? textColorDisabled : rgba(white, 0.6)

export const intents = new Map([ // intent: [default, hover, active, disabled]
  [Intent.STANDARD, [intentStandard, lightGray4, lightGray2, rgba(lightGray1, 0.5)]],
  [Intent.PRIMARY, [intentPrimary, blue2, blue1, rgba(intentPrimary, 0.5)]],
  [Intent.SUCCESS, [intentSuccess, green2, green1, rgba(intentSuccess, 0.5)]],
  [Intent.WARNING, [intentWarning, orange2, orange1, rgba(intentWarning, 0.5)]],
  [Intent.DANGER, [intentDanger, red2, red1, rgba(intentDanger, 0.5)]],
])
