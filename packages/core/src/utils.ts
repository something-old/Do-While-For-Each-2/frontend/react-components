import * as Color from 'color';

// https://www.npmjs.com/package/color
export const rgba = (hex, a) => {
  const color = Color(hex).alpha(a);
  return color.toString();
}
