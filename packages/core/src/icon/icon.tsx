import * as React from 'react';
import {Size} from '../variables';

interface IProps {
  icon?: string;
  iconSize?: number;
  size?: Size;
}


export const Icon = (props: IProps = {}) => {
  const {icon, iconSize} = props
  // viewBox={viewBox}
  return (
    <svg data-icon={icon} width={iconSize} height={iconSize}>
      {/*{paths}*/}
    </svg>
  )
}
