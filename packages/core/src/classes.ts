import {Intent, isIntentStd, isSizeStd, Size} from './variables';

export const ACTIVE = 'active'

export const intentClass = (intent?: Intent) => (
  !intent || isIntentStd(intent)
    ? ''
    : `intent-${intent}`
)

export const sizeClass = (size?: Size) => (
  !size || isSizeStd(size)
    ? ''
    : size
)
